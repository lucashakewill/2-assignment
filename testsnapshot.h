#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#define MAX_KEY_LENGTH 16
#define MAX_LINE_LENGTH 1024

typedef struct value value;
typedef struct entry entry;
typedef struct snapshot snapshot;

struct value
{
  value* prev;
  value* next;
  int value;
};

struct entry
{
  entry* prev;
  entry* next;
  value* values;
  char key[MAX_KEY_LENGTH];
};

struct snapshot
{
  snapshot* prev;
  snapshot* next;
  entry* entries;
  int id;
};

void bye();
void help();
bool checkentry();
bool checksnap();
void listkeys();
void listentries();
void listsnapshots();
void get(char *key);
void nokey();
entry* find_entry(char * check_key);
void set_entry(char *set_key, int *new_values, size_t arr_length);
void free_values(value *vals);
void *test_malloc(void *ptr);
value *set_values(value *vls, int *new_values, size_t arr_length);

#endif
