#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "testsnapshot.h"

entry* entries_head = NULL;
entry* entries_tail = NULL;
 


int main(void)
{
    char key[] = "key1";
    int values[] = {5, 12, 5, 26};
    int values2[] = {8, 0, 0, 8, 1, 3, 5};
    size_t len = 4;
    size_t len2 = 7;
    
    set_entry(key, values, len);

    get(key);
    get("nonexistentkey");
    

    set_entry(key, values2, len2);
    get(key);
    
return 0;

}



/* Display values for a given entry. Argument is the key that identifies the entry.

*/

void get(char *key)
{
  entry *tmp = NULL;
    bool success = false;

    
    if((tmp = find_entry(key))) // store the entry w given key in tmp
    {
      value *tmpval = tmp->values;

      printf("%s", "[");
      while (tmpval !=  NULL)
        {
          if (tmpval->next != NULL)
            printf("%d ", tmpval->value);

          else
            printf("%d", tmpval->value);

          tmpval = tmpval->next;
        }
	printf("%s", "]\n");
        success = true;
      }

    if (!success)
    {
        printf("%s\n", "[]");
    }
}





/* return the entry given a key. return null if the entry doesn't exist. */

entry* find_entry(char *check_key)
{

    entry *tmp = entries_head;

    while(tmp)
    {
        if ((strcmp(tmp->key, check_key))==0) //check if *key is equal to the key at this iteration
        {
            return tmp;
        }
            tmp = tmp->next;
    }

    return NULL;
      
}      



/* Create a new entry and insert at head of entries list. Parameters are the key and values. Used by SET command. note SET command edits if there is already an entry with the same key  */

void set_entry(char *set_key, int *new_values, size_t arr_length)
{
    entry *ptr = find_entry(set_key); //the entry to be modified or created
    
    if (!ptr) //if the key doesn't exist yet, so make a new entry
    {
        entry *entr = (entry*)test_malloc(malloc(sizeof(entry)));
        strcpy(entr->key, set_key);
    
        if (!entries_head) //first entry
        {
            entries_head = entr;
            entries_tail = entr;
            entries_head->prev = NULL;
            entries_head->next = NULL;
        }
    
        else //entries already exist
        {
            entr->next = entries_head;
            entr->prev = NULL; 
            entries_head->prev=entr;
            entries_head = entr;
        }
        
       entries_head->values = set_values(entries_head->values, new_values, arr_length);
      }
        
    else //key already exists. replace values with new values.
    {
       free_values(ptr->values);
        ptr->values = set_values(ptr->values, new_values, arr_length);
    }     
}



/* returns a values list populated with the values of a given int array. */

value *set_values(value *vls, int *new_values, size_t arr_length)
{
        value* val = NULL;
        value *recent = NULL; //most recently added value
        value *first = NULL;
    
        for (int i=0; i<(int)arr_length; i++) /*populate the linked list with the values stored in new_values*/
        {
            val = (value*)test_malloc(malloc(sizeof(value)));
            
            if (!first) //first value
            {
                
                val->value = new_values[i];
                val->next = NULL;
                val->prev = NULL;
                recent = val;
                first = val;
            }
            
            else //subsequent values
            {
                val->value = new_values[i];
                val->prev = recent;
                val->next = NULL;
                recent->next = val; 
                recent = val;
                
            }
            
        }
    return first;
        
}
  
/* delete a given values list and point it to null. */
void free_values(value *vals)
{
    
    value *tmp = vals;
    while (tmp)
    {
         if (tmp->next != NULL)
         {
            tmp=tmp->next;
            free(tmp->prev);
         }
            
        else 
            free(tmp);
            vals = NULL;
            break;
    }
    
    
    
}

/*check if malloc worked. if not, abort program. return the pointer given by malloc */
    
void *test_malloc(void *ptr) 
{
    if (!ptr) 
    {
        fprintf(stderr, "%s", "malloc failed");
        abort(); 
    }
    return ptr;
}



