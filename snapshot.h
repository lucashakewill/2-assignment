#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#define MAX_KEY_LENGTH 16
#define MAX_LINE_LENGTH 1024
#define MAX_COMMAND_LENGTH 16
#define NUMBER_OF_COMMANDS 25

/* indexes of the keys, ids, or values stored in the  tokens array, depending on what command is parsed */
#define CMD_INDEX 0
#define KEY_INDEX 1
#define ID_INDEX 1
#define VAL_INDEX 2
#define IND_INDEX 2
#define FIRST_VALUE_INDEX 1

typedef struct value value;
typedef struct entry entry;
typedef struct snapshot snapshot;

struct value
{
  value* prev;
  value* next;
  int value;
};

struct entry
{
  entry* prev;
  entry* next;
  value* values;
  char key[MAX_KEY_LENGTH];
};

struct snapshot
{
  snapshot* prev;
  snapshot* next;
  entry* entries;
  int id;
};

/* function declarations */

void bye();
void help();
void list_keys();
void list_entries();
void list_snapshots();
void get_entry(char *key);
entry *del_entry(char *key, entry *head);
void free_entry(entry *entr);
void purge_entry(char *key);
entry* find_entry(char * check_key, entry *head);
void set_entry(char *set_key, int *new_values, size_t arr_length, entry *head);
void push_entry(char *set_key, int *new_values, size_t arr_length);
void append_entry(char *set_key, int *new_values, size_t arr_length);
entry* pick_entry(char *set_key, int index);
bool pick_funct(char *set_key, int index);
void pluck_entry(char *set_key, int index);
void pop_entry(char * set_key);
void drop_entries(entry *to_del);
void drop_snapshot(int snap_id);
void save_snapshot();
void min_entry(char *set_key);
void max_entry(char *set_key);
void sum_entry(char *set_key);
void len_entry(char *set_key);
void rev_entry(char *set_key);
void uniq_entry(char *set_key);
void sort_entry(char *set_key);
void free_values(value *vals);
void *test_malloc(void *ptr);
value *set_values(value *vls, int *new_values, size_t arr_length);
void read_in();
void read_command();
enum COMMANDS command_parse(char* com);

/* help string */

const char* HELP =
    "BYE   clear database and exit\n"
    "HELP  display this help message\n"
    "\n"
    "LIST KEYS       displays all keys in current state\n"
    "LIST ENTRIES    displays all entries in current state\n"
    "LIST SNAPSHOTS  displays all snapshots in the database\n"
    "\n"
    "GET <key>    displays entry values\n"
    "DEL <key>    deletes entry from current state\n"
    "PURGE <key>  deletes entry from current state and snapshots\n"
    "\n"
    "SET <key> <value ...>     sets entry values\n"
    "PUSH <key> <value ...>    pushes each value to the front one at a time\n"
    "APPEND <key> <value ...>  append each value to the back one at a time\n"
    "\n"
    "PICK <key> <index>   displays entry value at index\n"
    "PLUCK <key> <index>  displays and removes entry value at index\n"
    "POP <key>            displays and removes the front entry value\n"
    "\n"
    "DROP <id>      deletes snapshot\n"
    "ROLLBACK <id>  restores to snapshot and deletes newer snapshots\n"
    "CHECKOUT <id>  replaces current state with a copy of snapshot\n"
    "SNAPSHOT       saves the current state as a snapshot\n"
    "\n"
    "MIN <key>  displays minimum entry value\n"
    "MAX <key>  displays maximum entry value\n"
    "SUM <key>  displays sum of entry values\n"
    "LEN <key>  displays number of entry values\n"
    "\n"
    "REV <key>   reverses order of entry values\n"
    "UNIQ <key>  removes repeated adjacent entry values\n"
    "SORT <key>  sorts entry values in ascending order\n";

/** Declare the commands avaliable

The reason you use struct is so you can just pass the struct on to the function that has the switch statement. It's classy. */

enum COMMANDS { CMD_BYE, CMD_HELP, // 0 1
               CMD_LIST_KEYS, CMD_LIST_ENTRIES, CMD_LIST_SNAPSHOTS, // 2 3 4
               CMD_GET, CMD_DEL, CMD_PURGE, CMD_SET, //5 6 7 8
               CMD_PUSH, CMD_APPEND, CMD_PICK, CMD_PLUCK, //9 10 11 12
               CMD_POP, CMD_DROP, CMD_ROLLBACK, CMD_CHECKOUT, //13 14 15 16
               CMD_SNAPSHOT, CMD_MIN, CMD_MAX, CMD_SUM, //17 18 19 20
               CMD_LEN, CMD_REV, CMD_UNIQ, CMD_SORT}; //21 22 23 24

const char *COMMAND_STR[] = {"BYE", "HELP\0", "LISTKEYS", "LISTENTRIES", "LISTSNAPSHOTS", "GET", "DEL", "PURGE", "SET", "PUSH", "APPEND", "PICK", "PLUCK", "POP", "DROP", "ROLLBACK", "CHECKOUT", "SNAPSHOT", "MIN", "MAX", "SUM", "LEN", "REV", "UNIQ", "SORT"};

/* global pointers */

entry* entries_head = NULL;
entry* entries_tail = NULL;
snapshot* snapshots_head = NULL;
snapshot* snapshots_tail = NULL;



#endif
