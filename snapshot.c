#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include "snapshot.h"




/* /labcommon/comp2129/bin/snapshot

Implement Snapshot DB

*/



int main(void)
{
    read_in();

}

/* Read commands and parameters from stdin and passes them as a char array to read_command*/

void read_in()
{
    char inputline[MAX_LINE_LENGTH];
    char* ptr = NULL;


    printf(">");
    while (fgets(inputline, MAX_LINE_LENGTH, stdin))
    {

        /*store the number of args in argc. store the arguments in argv, a dynamically sized array*/

        int argc = 0;
        size_t arr_size = 1;

        char ** argv = (char**)test_malloc(malloc(sizeof(char*) * arr_size));

        char *token = strtok_r(inputline, " \n", &ptr);

        while(token)
        {
            argv[argc] = token;
            argc++;

            if (argc == arr_size)
            {
             arr_size *=2;
             argv = test_malloc(realloc(argv, sizeof(char *) * arr_size));
            }
            token = strtok_r(NULL, " \n", &ptr);
        }


        read_command(argv, argc);

        free(argv);
        argv=NULL;
        printf(">");

      

    }

}


/*parse commands and parameters from individual entries on a string array*/

void read_command(char **in, int argc)
{
     /* store tokenised commands, keys and values */

    enum COMMANDS command; // store the command as an enumerated type
    //char key[MAX_KEY_LENGTH];
    //int values[MAX_LINE_LENGTH];
    char **input = in;
    int set_values[MAX_COMMAND_LENGTH];
    int index;

    //store the command in com

    char com[MAX_COMMAND_LENGTH];

    strcpy(com, input[CMD_INDEX]);

    /*if there's a second part of the command for LIST case*/
    if (strcasecmp(com, "LIST")==0)
    {
        strcat(com, in[CMD_INDEX + 1]);
    }

    command = command_parse(com);

    switch (command) //which command has been called
    {
        case CMD_BYE:
            bye(); //segfault, doesn't free properly
            free(in);
            in=NULL;
            exit(0);
            break;

        case CMD_HELP:
            help();
            break;

        case CMD_LIST_KEYS:
            list_keys();
            break;

        case CMD_LIST_ENTRIES:
            list_entries();
            break;

        case CMD_LIST_SNAPSHOTS:
            list_snapshots();
             break;

        case CMD_GET:
            get_entry(in[KEY_INDEX]);
            break;

        case CMD_DEL:
            entries_head=del_entry(in[KEY_INDEX], entries_head);
           
            break;

        case CMD_PURGE:
        purge_entry(in[KEY_INDEX]);
        printf("ok\n");
            break;

        case CMD_SET:
            for (int i=0; i<(argc-VAL_INDEX); i++) /* argc-VAL_INDEX is the number of integer values in the array, after the cmd and key*/
            {
                set_values[i] = atoi(in[(i+VAL_INDEX)]);
            }
            set_entry(in[KEY_INDEX], set_values, (size_t)(argc-VAL_INDEX), entries_head);
            printf("ok\n");
            break;

        case CMD_PUSH:

            for (int i=0; i<(argc-VAL_INDEX); i++) /* argc-VAL_INDEX is the number of integer values in the array, after                                                        the cmd and key*/
            {
                set_values[i] = atoi(in[(i+VAL_INDEX)]);
            }

            push_entry(in[KEY_INDEX], set_values, (size_t)(argc-VAL_INDEX));
            
            break;

        case CMD_APPEND:

            for (int i=0; i<(argc-VAL_INDEX); i++) /* argc-VAL_INDEX is the number of integer values in the array, after                                                        the cmd and key*/
            {
                set_values[i] = atoi(in[(i+VAL_INDEX)]);
            }

            append_entry(in[KEY_INDEX], set_values, (size_t)(argc-VAL_INDEX));
            

            break;

        case CMD_PICK:

            if (!in[IND_INDEX])
            {
                printf("pick needs two arguments\n");
                break;
            }
            index = atoi(in[IND_INDEX]);
            if(!pick_funct(in[KEY_INDEX], index))
            {
                printf("index out of range\n");
            }
            break;

        case CMD_PLUCK:

            if (!in[IND_INDEX])
            {
                printf("pluck needs two arguments\n");
                break;
            }
            index = atoi(in[IND_INDEX]);
            pluck_entry(in[KEY_INDEX], index);
            break;

        case CMD_POP:
            pop_entry(in[KEY_INDEX]);
             break;

        case CMD_DROP:
            index = atoi(in[ID_INDEX]);
            drop_snapshot(index);
            printf("ok\n"); //segfault
             break;

        case CMD_ROLLBACK:
            //
                 break;
        case CMD_CHECKOUT:
            //
             break;
        case CMD_SNAPSHOT:
            save_snapshot();
             break;

        case CMD_MIN:
            min_entry(in[KEY_INDEX]);
            break;

        case CMD_MAX:
            max_entry(in[KEY_INDEX]);
             break;
        case CMD_SUM:
            sum_entry(in[KEY_INDEX]);
            break;
        case CMD_LEN:
            len_entry(in[KEY_INDEX]);
             break;
        case CMD_REV:
            rev_entry(in[KEY_INDEX]);
             break;
        case CMD_UNIQ:
            uniq_entry(in[KEY_INDEX]);
             break;
        case CMD_SORT:
            sort_entry(in[KEY_INDEX]);
            break;

        default:
            fprintf(stderr, "%s\n", "couldn't parse command");
            break;
    }


}

/*check what command a string represents and return the required enum. Return -1 for failure */

enum COMMANDS command_parse(char* com)
{
    enum COMMANDS cmd;
    for (int i=0; i<NUMBER_OF_COMMANDS; i++)
    {
        if (strcasecmp(com, COMMAND_STR[i]) == 0)
        {
            cmd = i;
            return cmd;
        }

    }
    return 0;
}


/* exit the snapshot db program and clear all memory SEGFAULT */

void bye()
{
    if (snapshots_head) //there are snapshots. drop them.
    {
        snapshot* snap_del = snapshots_head;

        while (snap_del)
        {
            if (!snap_del->next) //last on the list
            {
                drop_snapshot(snap_del->id);
                break;
            }
            else if (!snap_del->prev) //first but not last
            {
                snap_del = snap_del->next;
            }

            else
            {
                drop_snapshot(snap_del->prev->id);
                snap_del = snap_del->next;
            }

        }
    }

    drop_entries(entries_head);
  printf("%s\n", "bye");


}


/*display all keys stored in the current state. CHECK WHILE LOOP */

void list_keys()
{

  if (!entries_head)
    printf("%s\n", "no keys");

  else
  {
    entry *tmp = entries_head;

    while (tmp)
    {
      printf("%s\n", tmp->key);
      tmp = tmp->next;
    }
  }
}

/* display all entries stored in the current state */

void list_entries()
{
  if (!entries_head)
    printf("%s\n", "no entries");
  else
  {
    entry *tmp = entries_head;

    while (tmp)
    {
      printf("%s [", tmp->key);

      value *tmpval = tmp->values;

      while (tmpval)
        {
          if (tmpval->next != NULL)
            printf("%d ", tmpval->value);

          else
          {
              printf("%d]\n", tmpval->value);
              break;
          }

          tmpval = tmpval->next;

        }
      tmp = tmp->next;
    }
  }
}


void list_snapshots()
{

  if (!snapshots_head)
    printf("%s\n", "no snapshots");

  else
  {
      snapshot *tmp = snapshots_head;

      while (tmp)
      {
        printf("%d ", tmp->id);
        tmp = tmp->next;
      }

      printf("\n");
  }
}

/* Display values for a given entry. Argument is the key that identifies the entry.*/

void get_entry(char *key)
{
  entry *tmp = NULL;
    bool success = false;
    tmp = find_entry(key, entries_head);
        
    if (!tmp)
    {
        printf("no such key\n");
    }

    else 
    {
      value *tmpval = tmp->values; 

      printf("%s", "[");
      while (tmpval !=  NULL)
        {
          if (tmpval->next != NULL)
            printf("%d ", tmpval->value);

          else
            printf("%d", tmpval->value);

          tmpval = tmpval->next;
        }
	printf("%s", "]\n");
        success = true;
      }

    /*if (!success)
    {
        printf("%s\n", "[]");
    }*/
}

/* delete an entry, given the key and the entries list it is stored in
return pointer to the new head of the list */

entry *del_entry(char *key, entry* head)
{
    entry *tmp = find_entry(key, head);

    if(tmp!=NULL)
    {
        if (!(tmp->prev)) //entry is head of the list
        {
            head = tmp->next;
        }

        else if ((tmp->prev !=NULL) && (tmp->next != NULL)) //somewhere in the middle
        {
            tmp->prev->next = tmp->next;
            tmp->next->prev = tmp->prev;
        }

        else //end of the list
        {
            tmp->prev->next = NULL;
        }

        free_entry(tmp);
        printf("ok\n");
    }

    else
    {
        printf("no such key\n");
    }

    return head;
}

void purge_entry(char *key)
{
    entries_head=del_entry(key, entries_head);

    if (snapshots_head)
    {
        snapshot *tmp = snapshots_head;

        while (tmp)
        {
            tmp->entries=del_entry(key, tmp->entries);
            tmp = tmp->next;
        }
    }
}

/* return the entry given a key and the head of the list (entries_head for the current list). return null if the entry doesn't exist. */

entry* find_entry(char *check_key, entry *head)
{

    entry *tmp = head;

    while(tmp)
    {
        if ((strncmp(tmp->key, check_key, (size_t)MAX_KEY_LENGTH))==0) //check if *key is equal to the key at this iteration
        {
            return tmp;
        }
            tmp = tmp->next;
    }

    return NULL;

}


/* Create a new entry and insert at head of a given entries list. Parameters are the key and values. Used by SET command. Note that if the key already exists, set_entry will update the values list but not create a new entry
*/

void set_entry(char *set_key, int *new_values, size_t arr_length, entry *head)
{
    entry *ptr = find_entry(set_key, head); //the entry to be modified or created

    if (!ptr) // the key doesn't exist yet, so make a new entry
    {
        size_t en = sizeof(entry);
        entry *entr = (entry*)test_malloc(malloc(en));
        strcpy(entr->key, set_key);

        if (!entries_head) //first entry
        {
            entries_head = entr;
            entries_tail = entr;
            entries_head->prev = NULL;
            entries_head->next = NULL;
        }

        else //entries already exist
        {
            entr->next = entries_head;
            entr->prev = NULL;
            entries_head->prev=entr;
            entries_head = entr;
        }

       entries_head->values = set_values(entries_head->values, new_values, arr_length);
      }

    else //key already exists. replace values with new values.
    {
       free_values(ptr->values);
        ptr->values = set_values(ptr->values, new_values, arr_length);
    }
}



/* returns a values list populated with the values of a given int array. */

value *set_values(value *vls, int *new_values, size_t arr_length)
{
        value* val = NULL;
        value *recent = NULL; //most recently added value
        value *first = NULL;

        for (int i=0; i<(int)arr_length; i++) /*populate the linked list with the values stored in new_values*/
        {
            val = (value*)test_malloc(malloc(sizeof(value)));

            if (!first) //first value
            {

                val->value = new_values[i];
                val->next = NULL;
                val->prev = NULL;
                recent = val;
                first = val;
            }

            else //subsequent values
            {
                val->value = new_values[i];
                val->prev = recent;
                val->next = NULL;
                recent->next = val;
                recent = val;

            }

        }
    return first;
}

/* insert values into existing values list, pushing each value to the front. */

void push_entry(char *set_key, int *new_values, size_t arr_length)
{
    entry *ptr = find_entry(set_key, entries_head); //the entry to be modified

    if (!ptr)
    {
        printf("no such key\n");
    }

    else
    {
        value* val = NULL;
        value *head = ptr->values; //head of values list to be altered


        for (int i=0; i<(arr_length); i++)
        {
            val = (value*)test_malloc(malloc(sizeof(value)));
            val->value = new_values[i];
            val->next = head;
            val->prev = NULL;
            if(head){
            head->prev=val;
            }
            head = val;
        }
        ptr->values = head;
        printf("ok\n");
    }
}

/* append elements to a values list */

void append_entry(char *set_key, int *new_values, size_t arr_length)

{
        entry *ptr = find_entry(set_key, entries_head); //the entry to be modified or created
        if(!ptr)
        {
            printf("no such key\n");
            return;
        }
        value * tmp = ptr->values;
        value* last = NULL; //will store the last entry in the list to be added to
        value* val = NULL;

    while(tmp)
    {
        last = tmp;
        tmp = tmp->next;
    }

        for (int i=0; i<(int)arr_length; i++) /*populate the linked list with the values stored in new_values*/
        {
            val = (value*)test_malloc(malloc(sizeof(value)));

            val->value = new_values[i];
            val->prev = last;
            val->next = NULL;
            last->next = val;
            last = val;

        }
    printf("ok\n");
}


/* return false if index out of range. for pick command */
bool pick_funct(char *set_key, int index)
{
     entry *ptr = find_entry(set_key, entries_head); //the entry to be modified or created
    if(!ptr)
        {
            printf("no such key\n");
            return true;
        }
    value * tmp = ptr->values;

    for (int i=1; tmp; i++) //entries are indexed from 1
    {
        if (i==index)
        {
            printf("%d\n", tmp->value);
            return true;
        }
        tmp = tmp->next;
    }
    
    return false;

}

void pluck_entry(char *set_key, int index)
{ 
    
    /* display the value */
    
    entry *ptr = find_entry(set_key, entries_head); //points to the entry at key
    
    if(!ptr)
        {
            printf("no such key\n");
            return;
        }
    
    value * val = ptr->values;
    bool success = false;

    //iterate through the values to find the value at that index
    for (int i=1; val; i++) //entries are indexed from 1
    {
        if (i==index)
        {
            printf("%d\n", val->value);
            success = true;
            break;
        }
        val = val->next;   
    }
    
    if(!success)
        {
            printf("index out of range\n");
            return;
        }


    if ((val->next != NULL) && (val->prev !=NULL)) /*middle of values*/
    {
        val->prev->next = val->next;
        val->next->prev = val->prev;

    }
    else if ((val->next !=NULL) && !(val->prev)) /* head of values*/
    {
    val->next->prev = NULL;
    ptr->values = val->next;
    }

    else if ((val->next == NULL) && (val->prev!=NULL)) /*tail */
    {
        val->prev->next = NULL;
    }

    else if ((!val->next) && (!val->prev)) /*only entry*/
    {
      ptr->values = NULL;
    }

    free(val);
    val = NULL;
}

void pop_entry(char * set_key)
{ 
    
    /* display the value */
    
    entry *ptr = find_entry(set_key, entries_head); //points to the entry at key
    
    if(!ptr)
        {
            printf("no such key\n");
            return;
        }
    
    value * val = ptr->values; //head of list
    
    if(!val)
        {
            printf("nil\n");
            return;
        }
    printf("%d\n", ptr->values->value);
    
    
    
    
    
    if ((val->next !=NULL) && !(val->prev)) /* head of values*/
    {
    val->next->prev = NULL;
    ptr->values = val->next;
    }

    else if ((val->next == NULL) && (val->prev!=NULL)) /*tail */
    {
        val->prev->next = NULL;
    }

    else if ((!val->next) && (!val->prev)) /*only entry*/
    {
      ptr->values = NULL;
    }

    free(val);
    val = NULL;
}

/*save the current working database as a snapshot */
void save_snapshot()
{
	snapshot* new_snap = (snapshot*)test_malloc(malloc(sizeof(snapshot)));
	new_snap->entries =  (entry*)test_malloc(malloc(sizeof(entry))); //alloc the head entry in the new_snap


/* iterate through and copy each entry */

	entry* entr_head = entries_head; //iterate through the old list
    entry* curr_entries = new_snap->entries; //iterate through the new list. points to head of new list
	entry* previous_entry = NULL; //save the previous entry to link up the list


   while(entr_head)
	{

		strcpy(curr_entries->key, entr_head->key); //copy key
		curr_entries->values = (value*)test_malloc(malloc(sizeof(snapshot))); //new values list for this entry


	/*iterate through the values list */

		value *val_head = entr_head->values; //pointer to old values list
		value *curr_vals = curr_entries->values;
		value *previous_val = NULL;

		while(val_head)
		{
			curr_vals->value = val_head->value;

			if (previous_val != NULL) //not at the head of the values list
			{
				previous_val->next = curr_vals;
			}

			curr_vals->prev = previous_val; //NULL if we're at the head

			previous_val = curr_vals;


			if (!val_head->next) /* if we're at the end of the list, set next value to null */
			{
				curr_vals->next = NULL;
			}

			else /*if we're not at the end of the list, allocate mem for the next value */
			{
				curr_vals->next = (value*)test_malloc(malloc(sizeof(snapshot)));
				curr_vals = curr_vals->next;
			}
			val_head = val_head->next;
		}


/* populate the entry variables correctly */
		if (previous_entry != NULL) //if we're not at the head of the list
		{

			previous_entry->next=curr_entries;
		}

			curr_entries->prev = previous_entry; //NULL if we're at the head of the list.
			previous_entry = curr_entries;


		if (!entr_head->next) /*if we're at the end of the list, set the next value to null. otherwise we will set next value in next iteration of the loop.*/
		{
		curr_entries->next = NULL;
		}

		else /* if we're not at the end of the list, allocate memory for the next entry */
		{
		curr_entries->next = (entry*)test_malloc(malloc(sizeof(entry)));
		curr_entries = curr_entries->next;
		}

		entr_head = entr_head->next;

	}

    /* add the snapshot to the snapshots list */
    int snapshot_id;
      if (!snapshots_tail) //if this is the first snapshot
    {
        snapshot_id=1;
        snapshots_head = new_snap;
        snapshots_tail = new_snap;
        new_snap->prev = NULL;
        new_snap->next = NULL;
     }

    else //there are other snapshots
    {
        new_snap->prev = NULL;
        new_snap->next = snapshots_head;
        snapshot_id= (snapshots_head->id + 1);
        new_snap->next->prev = new_snap;
        snapshots_head = new_snap;
    }

     new_snap->id = snapshot_id;
    printf("saved as snapshot %d\n", snapshot_id);

}

/*delete an entries list and all values stored in it*/
void drop_entries(entry *to_del)
{
     entry *entr = to_del;

        while (entr)
        {
            if (entr->next==NULL && entr->prev==NULL) //only entry on the list
            {
                free_entry(entr);
                entr=NULL;
                break;
            }

            else if ((!entr->prev) && (entr->next!=NULL)) //first entry on the list
            {
                entr = entr->next;
            }
            else if ((entr->prev != NULL) && (!entr->next)) //last entry
            {
              free_entry(entr->prev);
              free_entry(entr);
                entr=NULL;
                break;
            }
            else
            {
                free_entry(entr->prev);
                entr->prev=NULL;
                entr = entr->next;
            }
        }
}
/* delete a snapshot given the index, and free all its memory */
void drop_snapshot(int snap_id)
{
    snapshot *iter = snapshots_head;
    snapshot *to_del = NULL;

    while(iter) //find the requisite snapshot
    {
        if(iter->id == snap_id)
        {
            to_del = iter;
            break;
        }
        iter = iter->next;
    }

    if (!to_del)
        printf("no such snapshot\n");


    else
    {

        drop_entries(to_del->entries);

        //delete the actual snapshot

        if (!to_del->prev) //first on the list
        {
            snapshots_head = to_del->next;
        }

        if (to_del->next)
        {
            to_del->next->prev = to_del->prev;
        }

        if (to_del->prev)
        {
            to_del->prev->next = to_del->next;
        }

        free(to_del);
        to_del=NULL;

    }
}


/* print the minimum entry value in a given entry */

void min_entry(char *set_key)
{
      entry *ptr = find_entry(set_key, entries_head); //the entry to be modified or created
    if(!ptr)
        {
            printf("no such key\n");
            return;
        }
    value * tmp = ptr->values;

    int min = INT_MAX;
    for (int i=0; tmp; i++)
    {
        if (tmp->value < min)
        {
            min = tmp->value;
        }
        tmp = tmp->next;
    }
    printf("%d\n", min);

}

/* print the maximum entry value in a given entry*/

void max_entry(char *set_key)
{
    entry *ptr = find_entry(set_key, entries_head);
    if(!ptr)
        {
            printf("no such key\n");
            return;
        }
    value * tmp = ptr->values;

    int max = INT_MIN;
    for (int i=0; tmp; i++)
    {
        if (tmp->value > max)
        {
            max= tmp->value;
        }
        tmp = tmp->next;
    }
    printf("%d\n", max);

}

/* print the sum of the values stored in a given entry */
void sum_entry(char *set_key)
{
    entry *ptr = find_entry(set_key, entries_head);
    if(!ptr)
        {
            printf("no such key\n");
            return;
        }
    value *tmp = ptr->values;

    int sum = 0;
    for (int i=0; tmp; i++)
    {
        sum +=tmp->value;
        tmp = tmp->next;
    }
    printf("%d\n", sum);
}

/* print the number of values in a given entry */
void len_entry(char *set_key)
{
    entry *ptr = find_entry(set_key, entries_head);
    if(!ptr)
    {
        printf("no such key\n");
        return;
    }
    value *tmp = ptr->values;
    int i;

    for (i=0; tmp; i++)
    {
        tmp = tmp->next;
    }
    printf("%d\n", i);
}

/* reverse the values in a given entry */
void rev_entry(char *set_key)
{
    entry *ptr = find_entry(set_key, entries_head);

    if(!ptr)
    {
        printf("no such key\n");
        return;
    }
    value *val = ptr->values;
        //dynamic array to store the values
        size_t arr_size = 1;
        int *arr = (int*)test_malloc(malloc(sizeof(int) * arr_size));
        int count = 0; //how many values are stored in the dynamic array


        while(val)
        {
            arr[count] = val->value;
            val=val->next;
            count++;

            if (count== (int)arr_size)
            {
             arr_size *=2;
             arr = test_malloc(realloc(arr, (sizeof(int) * arr_size)));
            }
        }

    val = ptr->values; //go back to the start of the list

    int i = count-1;
    while(val)
    {
        val->value = arr[i];
        val = val->next;
        i--;
    }

    free(arr);
    arr=NULL;
    printf("ok\n");
}

/*remove repeated adjacent entry values*/
void uniq_entry(char *set_key)
{
    entry *ptr = find_entry(set_key, entries_head);

    if(!ptr)
    {
        printf("no such key\n");
        return;
    }

    value *val = ptr->values;
    value *previous = val; //first value
    val = val->next; //start with the second value.

    while(val)
    {
        if (val->value == previous->value) //remove previous
        {
            if ((previous->next != NULL) && (previous->prev !=NULL)) /*middle of values*/
            {
                previous->prev->next = previous->next;
                previous->next->prev = previous->prev;
            }

            else if ((previous->next !=NULL) && !(previous->prev)) /* head of values*/
            {
                previous->next->prev = NULL;
                ptr->values = previous->next;
            }

            else if ((previous->next == NULL) && (previous->prev!=NULL)) /*tail */
            {
                previous->prev->next = NULL;
            }

            free(previous);
            previous=NULL;

        }
            previous = val;
            val = val->next;
        }
    printf("ok\n");

    }

/*sort entry values in ascending order */
void sort_entry(char *set_key)
    
{
    entry *ptr = find_entry(set_key, entries_head);
    
    if(!ptr)
    {
        printf("no such key\n");
        return;
    }
   
    value *head = ptr->values;
    value *tmp = head;
    int count = 0;
    
    while (tmp) //how many values in the list
    {
        count++;
        tmp = tmp->next;
    }
    
    value *curr= NULL;
    value *trail = NULL;
    int tempval;
    
   
    for (int i=0; i<count; i++)
    {
        curr = head;
        trail = head;
        
        while(curr->next != NULL)
        {
            if (curr->value > curr->next->value) //swap em
            {
                tempval = curr->next->value;
                curr->next->value = curr->value; 
                curr->value = tempval;
            }
            trail = curr;
            curr = curr->next;
        }
      
    }
    printf("ok\n");
}
        
        
        
        
        
        
   /* do
    {
        if(!bubble->next) //reached end of list
        {
            bubble=ptr->values;
        }
        
        if((bubble->value) > (bubble->next->value))
        {
            storage = bubble->value;
            bubble->value = bubble->next->value;
            bubble->next->value = storage;
            swapped = true;
        }
        bubble = bubble->next;
        
    }
    while (swapped == true);*/


/* delete a given values list and point it to null. */

void free_values(value *vals)
{
    value *tmp = vals;
    while (tmp)
    {
         if (tmp->next != NULL)
         {
            tmp=tmp->next;
            free(tmp->prev);
             tmp->prev = NULL;
         }

        else
        {
            free(tmp);
            tmp = NULL;
            vals = NULL;
            return;
            
        }
    }
            
}


/* free a given entry and all values stored in it */

void free_entry(entry* entr)
{
    if (entr!=NULL)
    {
        if(entr->values !=NULL)
        {
        free_values(entr->values);
        }
    free(entr);
    entr = NULL;
  }
}

/*check if malloc worked. if not, abort program. return the pointer given by malloc */

void *test_malloc(void *ptr)
{
    if (!ptr)
    {
        fprintf(stderr, "malloc failed");
        abort();
    }
    return ptr;
}


/* print the text file called "help" in the same directory  */

void help()
{

    printf("%s", HELP);

}
